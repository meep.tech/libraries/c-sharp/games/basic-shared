﻿using Meep.Tech.ModularData;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Meep.Tech.Games.Basic.Geometry {

  /// <summary>
  /// 2D only coordinate (x, y, z = y)
  /// </summary>
  public struct Coordinate : IEquatable<Coordinate> {

    /// <summary>
    /// Zero
    /// </summary>
    public static Coordinate Zero
      = new Coordinate().initialize();

    /// <summary>
    /// east west
    /// </summary>
    public int x {
      get;
      set;
    }

    /// <summary>
    /// north south
    /// </summary>
    public int z {
      get;
      set;
    }

    /// <summary>
    /// up down in 2D (North south)
    /// </summary>
    public int y {
      get => z;
      set => z = value;
    }

    /// <summary>
    /// If this coordinate is valid and was properly initialized
    /// </summary>
    public bool isInitialized {
      get;
      set;
    }

    /// <summary>
    /// Create a 2d coordinate
    /// </summary>
    public Coordinate(int x, int z) {
      this.x = x;
      this.z = z;
      isInitialized = true;
    }

    /// <summary>
    /// Inline initialization
    /// </summary>
    /// <returns></returns>
    Coordinate initialize() {
      isInitialized = true;
      return this;
    }

    #region ICoordinate

    /// <summary>
    /// preform the acton on all coordinates between this one and the end coordinate
    /// </summary>
    /// <param name="end">The final point to run on, exclusive</param>
    /// <param name="action">the function to run on each point</param>
    /// <param name="step">the value by which the coordinate values are incrimented</param>
    public void until(Coordinate end, Action<Coordinate> action, int step = 1) {
      Coordinate current = (x, z);
      for (current.x = x; current.x < end.x; current.x += step) {
        for (current.z = z; current.z < end.z; current.z += step) {
          action(current);
        }
      }
    }

    /// <summary>
    /// preform the acton on all coordinates between this one and the end coordinate
    /// </summary>
    /// <param name="end">The final point to run on, exclusive</param>
    /// <param name="action">the function to run on each point</param>
    /// <param name="step">the value by which the coordinate values are incrimented</param>
    public void until(Coordinate end, Func<Coordinate, bool> action, int step = 1) {
      Coordinate current = (x, z);
      for (current.x = x; current.x < end.x; current.x += step) {
        for (current.z = z; current.z < end.z; current.z += step) {
          if (!action(current)) return;
        }
      }
    }

    /// <summary>
    /// Checks if this coordinate is greater than at least one of the bounds and thus 'beyond' the boundary
    /// </summary>
    public bool goesBeyond(Coordinate bounds) {
      return x > bounds.x
        || z > bounds.z;
    }

    /// <summary>
    /// Checks if this coordinate is greater than (or equal to) a lower bounds coordinate (Inclusive)
    /// </summary>
    public bool isAtLeastOrBeyond(Coordinate bounds) {
      return x >= bounds.x
        && z >= bounds.z;
    }

    /// <summary>
    /// Checks if this coordinate is within a bounds coodinate (exclusive)
    /// </summary>
    public bool isWithin(Coordinate bounds) {
      return x < bounds.x
        && z < bounds.z;
    }

    /// <summary>
    /// Checks if this coordinate is within a set, dictated by boundaries. (inclusive, and exclusive)
    /// </summary>
    public bool isWithin((Coordinate minInclusive, Coordinate maxExclusive) bounds) {
      return isWithin(bounds.minInclusive, bounds.maxExclusive);
    }

    /// <summary>
    /// Checks if this coordinate is within a set, dictated by boundaries. (inclusive, and exclusive)
    /// </summary>
    /// <param name="start">The starting location to check if the point is within, inclusive</param>
    /// <param name="bounds">The outer boundary to check for point inclusion. Exclusive</param>
    /// <returns></returns>
    public bool isWithin(Coordinate start, Coordinate bounds) {
      return isWithin(bounds) && isAtLeastOrBeyond(start);
    }

    /// <summary>
    /// Checks if this coordinate is within a set, dictated by boundaries. (inclusive, and exclusive)
    /// </summary>
    /// <param name="bounds">The inner [0](inclusive) and outer [1](exclusive) boundary to check for point inclusion</param>
    /// <returns></returns>
    public bool isWithin(Coordinate[] bounds) {
      if (bounds != null && bounds.Length == 2) {
        return isWithin(bounds[1]) && isAtLeastOrBeyond(bounds[0]);
      } else throw new ArgumentOutOfRangeException("Coordinate.isWithin must take an array of size 2.");
    }

    #endregion

    #region Comparison and Conversion

    /// <summary>
    /// get this as a cube coord, assuming it's an axial coord (used for hexagons)
    /// </summary>
    public Point Cube
      => (x, -x - z, z);

    /// <summary>
    /// Get this 2d coordinate as a 1D index.
    /// </summary>
    /// <param name="diameter2d">How wide the 2D cube is we're converting to 1D coordinates from</param>
    public int to1d(int diameter2d = 10000, int negativesBuffer = 100)
      => (y + negativesBuffer) * diameter2d + (x + negativesBuffer);

    /// <summary>
    /// Get a coordinate from a 1D index given the expected diameter of the cube the coordinates are for
    /// </summary>
    /// <param name="index1d">the 1d index</param>
    /// <param name="diameter2d">the diameter of the cube the 2d coordinates are for</param>
    /// <returns></returns>
    public static Coordinate From1d(int index1d, int diameter2d = 10000, int negativesBuffer = 100)
      => (index1d % diameter2d - negativesBuffer, index1d / diameter2d - negativesBuffer);

    /// implicit opperators
    ///===================================

    /// <summary>
    /// Create a coordinate from a touple.
    /// </summary>
    /// <param name="coordinates">(X, Z)</param>
    public static implicit operator Coordinate((int, int) coordinates) {
      return new Coordinate(coordinates.Item1, coordinates.Item2);
    }

    /// <summary>
    /// Create a coordinate from a touple.
    /// </summary>
    /// <param name="coordinates">(X, Z)</param>
    public static implicit operator Coordinate(Tuple<int, int> coordinates) {
      return new Coordinate(coordinates.Item1, coordinates.Item2);
    }

    /// <summary>
    /// Create a coordinate from a touple.
    /// </summary>
    /// <param name="coordinates">(X, Z)</param>
    public static implicit operator (int, int)(Coordinate coordinate) {
      return (coordinate.x, coordinate.z);
    }

    /// <summary>
    /// Create a coordinate from a point.
    /// </summary>
    /// <param name="points">(X, Z)</param>
    public static implicit operator Coordinate(Point point) {
      return (point.x, point.z);
    }

    /// OPPERATOR OVERRIDES
    ///===================================

    // ==
    public static bool operator ==(Coordinate a, Coordinate b) {
      return a.Equals(b);
    }

    // !=
    public static bool operator !=(Coordinate a, Coordinate b) {
      return !a.Equals(b);
    }

    // >
    public static bool operator >(Coordinate a, Coordinate b) {
      return a.x > b.x || a.y > b.y && a.z > b.z;
    }

    // <
    public static bool operator <(Coordinate a, Coordinate b) {
      return a.x < b.x && a.y < b.y && a.z < b.z;
    }

    /// math
    public static Coordinate operator +(Coordinate a, Coordinate b) {
      return (
        a.x + b.x,
        a.z + b.z
      );
    }
    public static Coordinate operator +(Coordinate a, int b) {
      return (
        a.x + b,
        a.z + b
      );
    }
    public static Coordinate operator -(Coordinate a, Coordinate b) {
      return (
        a.x - b.x,
        a.z - b.z
      );
    }
    public static Coordinate operator -(Coordinate a, int b) {
      return (
        a.x - b,
        a.z - b
      );
    }
    public static Coordinate operator *(Coordinate a, Coordinate b) {
      return (
        a.x * b.x,
        a.z * b.z
      );
    }
    public static Coordinate operator *(Coordinate a, int b) {
      return (
        a.x * b,
        a.z * b
      );
    }

    /// <summary>
    /// equals
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj) {
      return obj is Point coordinate &&
             x == coordinate.x &&
             z == coordinate.z;
    }

    /// <summary>
    /// equals override
    /// </summary>
    /// <param name="other"></param>
    /// <returns></returns>
    public bool Equals(Coordinate other) {
      return other.x == x && other.z == z;
    }

    /// <summary>
    /// deconstruct
    /// </summary>
    /// <param name="x"></param>
    /// <param name="z"></param>
    public void Deconstruct(out int x, out int z) {
      x = this.x;
      z = this.z;
    }

    #endregion

    #region Serialization

    /// <summary>
    /// Custom json serializer for coordinate fields
    /// </summary>
    public class JsonModelDataFieldAttribute : CustomModelDataFieldAttribute {
      public JsonModelDataFieldAttribute(string FieldName = null, bool SkipDuringDeserialization = false)
        : base(FieldName, SkipDuringDeserialization, false) { }

      public override Func<object, object> SerializationFunction
        => coord => JObject.FromObject(new Dictionary<string, int> {
          {"x", ((Coordinate)coord).x },
          {"y", ((Coordinate)coord).z }
        });

      protected override Func<JToken, object> DeserializeJSON 
        => json => (json.Value<int>("x"), json.Value<int>("y"));
    }

    /// <summary>
    /// Custom int/sql data serializer for coordinate fields
    /// </summary>
    public class SqlModelDataFieldAttribute : CustomModelDataFieldAttribute {
      readonly int cubeDiameter;
      readonly int negativeBuffer;

      public SqlModelDataFieldAttribute(string FieldName = null, int cubeDiameter = 100000, int negativeBuffer = 100, bool SkipDuringDeserialization = false) 
        : base(
            FieldName, 
            SkipDuringDeserialization,
            true
      ) {
        this.cubeDiameter = cubeDiameter;
        this.negativeBuffer = negativeBuffer;
      }

      public override Type CustomSerializeToType
        => typeof(int);

      public override Func<object, object> SerializationFunction
        => coord => coord is Coordinate coordinate
          ? coordinate.to1d(cubeDiameter, negativeBuffer)
          : 0;

      protected override Func<object, object> DeserializeObject
        => value => value is int coord1D
          ? From1d(coord1D, cubeDiameter, negativeBuffer)
          : default;
    }

    /// <summary>
    /// Is limited to (int.MAX, int.MAX)
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode() {
      return (z << 16) | x;
    }

    /// <summary>
    /// Coord to string
    /// </summary>
    /// <returns></returns>
    public override string ToString() {
      return "{" + x + ", " + z + "}";
    }

    #endregion
  }
}