﻿using System;

namespace Meep.Tech.Games.Basic.Geometry {

  /// <summary>
  /// Point in 3d space x, y, z
  /// </summary>
  public struct Point : IEquatable<Point> {

    /// <summary>
    /// Zero
    /// </summary>
    public static Point Zero 
      = new Point().initialize();

    /// <summary>
    /// east west
    /// </summary>
    public int x {
      get;
      set;
    }

    /// <summary>
    /// north south
    /// </summary>
    public int z {
      get;
      set;
    }

    /// <summary>
    /// up down in 2D (North south)
    /// </summary>
    public int y {
      get;
      set;
    }

    #region Initialization

    /// <summary>
    /// If this point is valid and was properly initialized
    /// </summary>
    public bool isInitialized {
      get;
      set;
    }

    /// <summary>
    /// Inline initialization
    /// </summary>
    /// <returns></returns>
    Point initialize() {
      isInitialized = true;
      return this;
    }

    /// <summary>
    /// Make a point in spa ce
    /// </summary>
    public Point(int x, int y, int z) {
      this.x = x;
      this.y = y;
      this.z = z;
      isInitialized = true;
    }

    #endregion

    #region Access and manipulation

    /// <summary>
    /// preform the acton on all points between this one and the end point
    /// </summary>
    /// <param name="end">The final point to run on, exclusive</param>
    /// <param name="action">the function to run on each point</param>
    /// <param name="step">the value by which the point values are incrimented</param>
    public void until(Point end, Action<Point> action, int step = 1) {
      Point current = (x, y, z);
      for (current.x = x; current.x < end.x; current.x += step) {
        for (current.y = y; current.y < end.y; current.y += step) {
          for (current.z = z; current.z < end.z; current.z += step) {
            action(current);
          }
        }
      }
    }

    /// <summary>
    /// preform the acton on all points between this one and the end point
    /// </summary>
    /// <param name="end">The final point to run on, exclusive</param>
    /// <param name="action">the function to run on each point</param>
    /// <param name="step">the value by which the point values are incrimented</param>
    public void until(Point end, Func<Point, bool> action, int step = 1) {
      Point current = (x, y, z);
      for (current.x = x; current.x < end.x; current.x += step) {
        for (current.y = y; current.y < end.y; current.y += step) {
          for (current.z = z; current.z < end.z; current.z += step) {
            if (!action(current)) return;
          }
        }
      }
    }

    #endregion

    #region Comparison and Conversion

    /// <summary>
    /// Create a point from a touple.
    /// </summary>
    /// <param name="points">(X, Z)</param>
    public static implicit operator Point((int, int, int) points) {
      return new Point(points.Item1, points.Item2, points.Item3);
    }

    /// <summary>
    /// Create a point from a touple.
    /// </summary>
    /// <param name="points">(X, Z)</param>
    public static implicit operator (int, int, int)(Point point) {
      return (point.x, point.y, point.z);
    }

    /// <summary>
    /// Create a point from a coordinate.
    /// </summary>
    /// <param name="points">(X, Z)</param>
    public static implicit operator Point(Coordinate coordinate) {
      return new Point(coordinate.x, 0, coordinate.z);
    }

    /// <summary>
    /// Checks if this point is greater than (or equal to) a lower bounds point (Inclusive)
    /// </summary>
    public bool isAtLeastOrBeyond(Point bounds) {
      return x >= bounds.x
        && y >= bounds.y
        && z >= bounds.z;
    }

    /// <summary>
    /// Checks if this point is within a bounds coodinate (exclusive)
    /// </summary>
    public bool isWithin(Point bounds) {
      return x < bounds.x
        && y < bounds.y
        && z < bounds.z;
    }

    /// <summary>
    /// Checks if this point is within a set, dictated by boundaries. (inclusive, and exclusive)
    /// </summary>
    public bool isWithin((Point minInclusive, Point maxExclusive) bounds) {
      return isWithin(bounds.minInclusive, bounds.maxExclusive);
    }

    /// <summary>
    /// Checks if this point is within a set, dictated by boundaries. (inclusive, and exclusive)
    /// </summary>
    /// <param name="start">The starting location to check if the point is within, inclusive</param>
    /// <param name="bounds">The outer boundary to check for point inclusion. Exclusive</param>
    /// <returns></returns>
    public bool isWithin(Point start, Point bounds) {
      return isWithin(bounds) && isAtLeastOrBeyond(start);
    }

    /// <summary>
    /// Checks if this point is within a set, dictated by boundaries. (inclusive, and exclusive)
    /// </summary>
    /// <param name="bounds">The inner [0](inclusive) and outer [1](exclusive) boundary to check for point inclusion</param>
    /// <returns></returns>
    public bool isWithin(Point[] bounds) {
      if (bounds != null && bounds.Length == 2) {
        return isWithin(bounds[1]) && isAtLeastOrBeyond(bounds[0]);
      } else throw new ArgumentOutOfRangeException("Point.isWithin must take an array of size 2.");
    }

    // ==
    public static bool operator ==(Point a, Point b) {
      return a.Equals(b);
    }

    // !=
    public static bool operator !=(Point a, Point b) {
      return !a.Equals(b);
    }

    // >
    public static bool operator >(Point a, Point b) {
      return a.x > b.x || a.y > b.y && a.z > b.z;
    }

    // <
    public static bool operator <(Point a, Point b) {
      return a.x < b.x && a.y < b.y && a.z < b.z;
    }

    /// <summary>
    /// equals
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj) {
      return obj is Point point &&
             x == point.x &&
             y == point.y &&
             z == point.z;
    }

    /// <summary>
    /// equals override
    /// </summary>
    /// <param name="other"></param>
    /// <returns></returns>
    public bool Equals(Point other) {
      return other.x == x && other.y == y && other.z == z;
    }

    /// <summary>
    /// deconstruct
    /// </summary>
    /// <param name="x"></param>
    /// <param name="z"></param>
    public void Deconstruct(out int x, out int y, out int z) {
      x = this.x;
      y = this.y;
      z = this.z;
    }

    /// <summary>
    /// Coord to string
    /// </summary>
    /// <returns></returns>
    public override string ToString() {
      return "{" + x + ", " + y + ", " + z + "}";
    }

    #endregion
  }
}
